## Task List ##

Who           | Done |Task
-------------:| ----:|-----------------
Luuk          |    X | Design + master
Luuk          |    X | Register
Luuk          |    X | Login
Luuk          |    X | Products
~~Mark~~ Luuk |    X | Account
~~Mark~~ Luuk |    X | Cart
~~Mark~~ Luuk |    X | Orderhistory
????          |      | BONUS: Password Recovery
????          |      | BONUS: Username Recovery?
Luuk          |    X | Database
Luuk          |    X | Errorpages
Luuk          |    X | GUID
~~Mark~~ Luuk |    X | Password hash
~~Mark~~ Luuk |    X | HTTPS
Luuk          |    X | Brute Force Protection
Luuk          |    X | XSRF/CSRF Protection
Luuk          |    / | Product filters

BONUS: Username Recovery is not possible, as the email address will be used for login.


#### Brute force protection examples:####
* Captcha
* Two second delay between pageload-verify.

#### Useful resources:####
* [OWASP Top 10](https://www.owasp.org/index.php/Top_10_2013-Top_10)
* [Readme Markdown](https://bitbucket.org/tutorials/markdowndemo)