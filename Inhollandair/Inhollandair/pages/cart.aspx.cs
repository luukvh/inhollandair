﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace Inhollandair.pages
{
    public partial class cart : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            int amount = 0, price = 0, priceMiles = 0;
            if (!this.IsPostBack)
            {
                string flightId = Request.QueryString["product"];
                if (!flightId.Equals(null))
                {
                    gridCart.DataSource = getFlightInfo(flightId, out price, out priceMiles);
                    gridCart.DataBind();

                    TextBox txtAmount = (TextBox)gridCart.Rows[0].Cells[0].FindControl("txtAmount");
                    amount = Convert.ToInt32(Request.QueryString["amount"]);
                    if (amount == 0) amount = 1;
                    txtAmount.Text = amount.ToString();

                    lblPrice.Text = "€ " + (price * amount).ToString();
                    lblPriceMiles.Text = (priceMiles * amount).ToString();
                }
                else ((Label)Master.FindControl("lblError")).Text = string.Format("U heeft geen order geplaatst");
            }
        }

        private DataTable getFlightInfo(string flightId, out int price, out int priceMiles)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("datum");
            dt.Columns.Add("vanaf");
            dt.Columns.Add("bestemming");
            dt.Columns.Add("prijs");
            dt.Columns.Add("InhollandMiles");

            string[] rowArray = DbFlightInfo(flightId);
            DataRow dr = dt.NewRow();
            dr["datum"] = rowArray[0];
            dr["vanaf"] = rowArray[1];
            dr["bestemming"] = rowArray[2];
            dr["prijs"] = rowArray[3];
            dr["InhollandMiles"] = rowArray[4];
            dt.Rows.Add(dr);

            price = Convert.ToInt32(rowArray[3]);
            priceMiles = Convert.ToInt32(rowArray[4]);
            return dt;
        }

        private string[] DbFlightInfo(string flightId)
        {
            string[] rowArray = new string[5];

            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT date, origin, destination, price, inhollandMiles FROM flights WHERE flightId = @Id;", conn);
                cmd.Parameters.AddWithValue("@Id", flightId);

                reader = cmd.ExecuteReader();
                reader.Read();

                for (int i = 0; i < 5; i++)
                {
                    rowArray[i] = reader.GetString(i);
                }
                return rowArray;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        protected void btnOrder_Click(object sender, EventArgs e)
        {
            HttpCookie userCookie = Request.Cookies["userCookie"];
            if (userCookie == null) Response.Redirect("login.aspx?" + Request.QueryString);    //user has to be logged in to place an order
            else
            {
                int price;
                AddOrder("geld", out price);
                //payment
                Response.Redirect("orderHistory.aspx");
            }
        }

        protected void btnOrderMiles_Click(object sender, EventArgs e)
        {
            HttpCookie userCookie = Request.Cookies["userCookie"];
            if (userCookie == null) Response.Redirect("login.aspx");    //user has to be logged in to place an order
            else
            {
                int userMiles = getUserMiles(userCookie.Value);

                if (userMiles < Convert.ToInt32(lblPriceMiles.Text)) ((Label)Master.FindControl("lblError")).Text = string.Format("tekort saldo ({0})", userMiles.ToString());
                else
                {
                    int price;
                    AddOrder("InhollandMiles", out price);
                    DecreaseUserMiles(price);
                    Response.Redirect("orderHistory.aspx");
                }
            }

        }

        private int getUserMiles(string userId)
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT inhollandMiles FROM users WHERE userId = @Id;", conn);
                cmd.Parameters.AddWithValue("@Id", userId);

                reader = cmd.ExecuteReader();
                reader.Read();
                return Convert.ToInt32(reader.GetString(0));
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        protected void txtAmount_Changed(object sender, EventArgs e)    //this event will only be launched when autopostback on the textbox is enabled
        {
            int amount;
            TextBox txtAmount = sender as TextBox;

            if (!Int32.TryParse(txtAmount.Text, out amount)) ((Label)Master.FindControl("lblError")).Text = string.Format("Incorrect aantal.");
            else Response.Redirect(String.Format("{0}?product={1}&amount={2}", Request.Url.GetLeftPart(UriPartial.Path), Request.QueryString["product"], txtAmount.Text));
        }

        private void AddOrder(string orderType, out int price)
        {
            int amount = Convert.ToInt32(Request.QueryString["amount"]);
            if (amount < 1) amount = 1;

            price = getPrice(orderType) * amount;       //get the price from the server again, so the user can't modify it.
            
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                string query = "INSERT INTO orders (`orderId`, `userId`, `price`, `flightId`, `paymentType`, `status`, `amount`) " +
                                            "VALUES(@OrderId , @UserId , @Price , @FlightId , @PaymentType , @Status , @Amount);";
                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@OrderId", getUnusedGuid(conn));
                cmd.Parameters.AddWithValue("@UserId", Request.Cookies["userCookie"].Value);
                cmd.Parameters.AddWithValue("@Price", price);
                cmd.Parameters.AddWithValue("@FlightId", Request.QueryString["product"]);
                cmd.Parameters.AddWithValue("@PaymentType", orderType);
                cmd.Parameters.AddWithValue("@Status", "ordered");
                cmd.Parameters.AddWithValue("@Amount", amount);

                cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                return;
            }
            finally //close all open connections
            {
                //no reader
                if (conn != null) conn.Close();
            }
        }

        private int getPrice(string orderType)
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                string query;
                if (orderType.Equals("geld")) query = "SELECT price FROM flights WHERE flightId = @Id";
                else query = "SELECT inhollandMiles FROM flights WHERE flightId = @Id";

                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Id", Request.QueryString["product"]);

                reader = cmd.ExecuteReader();
                reader.Read();
                return Convert.ToInt32(reader.GetString(0));
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private Guid getUnusedGuid(MySqlConnection conn)
        {
            bool used = true;
            Guid guid = Guid.NewGuid();
            string query = "SELECT orderId FROM orders WHERE orderId  = @OrderId;";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader reader;

            while (used)
            {
                cmd.Parameters.AddWithValue("@OrderId", guid);
                reader = cmd.ExecuteReader();

                if (!reader.HasRows) used = false;
                else guid = Guid.NewGuid();

                reader.Close();
            }

            return guid;
        }

        private void DecreaseUserMiles(int totalPriceMiles)
        {
            int userMiles = getUserMiles(Request.Cookies["userCookie"].Value) - totalPriceMiles;

            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("UPDATE users SET inhollandMiles = @InhollandMiles WHERE userId = @Id;", conn);
                cmd.Parameters.AddWithValue("@InhollandMiles", userMiles);
                cmd.Parameters.AddWithValue("@Id", Request.Cookies["userCookie"].Value);

                reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                return;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }
    }
}