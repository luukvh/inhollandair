﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="privacyPolicy.aspx.cs" Inherits="Inhollandair.pages.privacy_policy" %>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    Inhollandair - Privacy Policy
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <strong>inhollandair.nl - Privacy Policy:</strong>
    <p style='margin-top: 20px;'>01 Maart, 2016</p><br />
    <p>
        inhollandair.nl respecteert de privacy van alle gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie die u ons verschaft altijd vertrouwelijk wordt behandeld. 
        Wij gebruiken uw gegevens alleen om diensten waar u om gevraagd heeft te voldoen. 
    </p>
    <br />
    <br />
    <p>
        <strong>Uw persoonlijke gegevens: </strong>
    </p>
    <p>
        We zullen uw persoonlijke gegevens zonder uw toestemming niet ter beschikking stellen aan derden. We waarderen het vertrouwen dat u in ons stelt, en 
        zullen alles in het werk stellen om uw persoonlijke informatie te beschermen. De persoonlijke informatie wordt enkel en alleen 
        gebruikt voor de diensten waarvoor u ze aan ons toevertrouwd. Door ons een email te sturen kunt u uw gegevens laten verwijderen.
    </p><br />
    <p>
        inhollandair.nl maakt gebruik van cookies en andere technologieën om het online gebruiksgemak voor u te vergroten en om na te gaan hoe de site gebruikt, 
        zodat wij op basis daarvan de kwaliteit van onze diensten kunnen verbeteren. Door de cookies kunnen we verschillende advertenties laten zien, advertentie netwerken (oa Google Adsense) die op deze site adverteren kunnen
        cookies op uw pc plaatsen of kunnen web beacons gebruiken om informatie in te winnen. inhollandair.nl heeft geen toegang tot of controle over deze cookies die worden gebruikt door derden adverteerders.
    </p><br />
    <p>
        U moet het privacy beleid van deze derden raadplegen voor meer gedetailleerde informatie over hun handelen, alsook voor instructies over hoe de opt-out van bepaalde zaken werkt. inhollandair.nl privacy beleid is hier niet
        van toepassing op, we hebben geen controle over de activiteiten van deze andere adverteerders of websites. 
    </p>
    <p>
        Bij een bezoek aan onze website of bij gebruik van sommige van onze producten slaan de servers automatisch informatie op, zoals URL, IP-adres, browsertype en -taal, en datum en tijd van uw bezoek. 
    </p><br />
    <p>
        Als u cookies wilt uitschakelen, kunt u dit doen via uw browser. Meer gedetailleerde informatie over het beheer van cookies met specifieke webbrowsers kunnen worden gevonden op de browsers betreffende websites.
    </p><br />
    <ul>
        <li><a href=http://www.microsoft.com/info/cookies.htm rel=nofollow>Internet Explorer</a></li>
        <li><a href=http://www.mozilla.org/projects/security/pki/psm/help_21/using_priv_help.html rel=nofollow>Firefox</a></li>
        <li><a href=http://docs.info.apple.com/article.html?path=Safari/2.0/nl/ibr30.html rel=nofollow>Safari</a></li>
    </ul><br />
    <p>
        Heeft u suggesties, klachten, of vragen over onze Privay Policy? Stuur een e-mail naar <a href="mailto:support@inhollandair.nl">support@inhollandair.nl</a>
    </p>
</asp:Content>
