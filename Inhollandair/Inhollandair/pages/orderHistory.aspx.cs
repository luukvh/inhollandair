﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Web;

namespace Inhollandair.pages
{
    public partial class orderHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie userCookie = Request.Cookies["userCookie"];
            if (userCookie == null) Response.Redirect("login.aspx");
            else
            {
                DbUserOrders(userCookie.Value);
            }
        }

        private void DbUserOrders(string userId)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                //orderstatus is not used ATM. It should display if the user has canceled its own tickets. flightstatus is active/cancelled/expired (past date).
                string query = "SELECT f.date as vluchtdatum, f.origin as vanaf, f.destination as bestemming, o.amount as aantal, o.price as prijs, o.paymentType as betaalmethode, f.status as vluchtstatus " +
                                "FROM orders AS o INNER JOIN flights AS f ON o.flightId = f.flightId " +
                                "WHERE o.userId = @Id " +
                                "ORDER BY f.DATE;";
                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Id", userId);
                
                dt.Load(cmd.ExecuteReader());
                gridHistory.DataSource = dt;
                gridHistory.DataBind();

            }
            catch (Exception ex)
            {
                return;
            }
            finally //close all open connections
            {
                //no datareader
                if (conn != null) conn.Close();
            }
        }
    }
}