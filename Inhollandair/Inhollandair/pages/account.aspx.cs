﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inhollandair.pages
{
    public partial class account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Cookies["userCookie"] == null) Response.Redirect("login.aspx");
                string userId = Request.Cookies["userCookie"].Value;

                var Admin = new admin();
                if (Admin.CheckIsAdmin(userId)) lblAdmin.Visible = true;

                    if (userId == null) Response.Redirect("login.aspx");
                else if (Request.QueryString["mode"] == "edit")
                {
                    tabData.Visible = false;
                    TabEdit.Visible = true;

                    //fill the labels with database data
                    string[] userData = getUserData(userId);
                    txtEmail.Text = userData[0];
                    txtFirstname.Text = userData[1];
                    txtLastname.Text = userData[2];
                    txtDate.Text = userData[3];
                    txtAddress.Text = userData[4];
                    txtZipcode.Text = userData[5];
                    txtCity.Text = userData[6];
                }
                else
                {
                    tabData.Visible = true;
                    TabEdit.Visible = false;

                    //fill the textboxes with database data
                    string[] userData = getUserData(userId);
                    lblEmail.Text = userData[0];
                    lblName.Text = String.Format("{0} {1}", userData[1], userData[2]);
                    lblBirthday.Text = userData[3];
                    lblAdres.Text = userData[4];
                    lblZipcode.Text = userData[5];
                    lblCity.Text = userData[6];
                    lblMiles.Text = userData[7];
                }
            }
        }

        

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            string userId = Request.Cookies["userCookie"].Value;
            string[] userData = getUserData(Request.Cookies["userCookie"].Value);


            if (!ComparePasswords(userId, txtOldPass.Text, 1))  //check if the old password entered is correct.
            {
                ((Label)Master.FindControl("lblError")).Text = string.Format("Incorrect oud wachtwoord.");
                return;
            }

            if (!txtPass.Text.Equals(""))
            {
                if (txtPassAgain.Text.Equals(""))
                {
                    ((Label)Master.FindControl("lblError")).Text = string.Format("U moet uw nieuwe wachtwoord nogmaals invullen om te wijzigen.");
                    return;
                }
                else if (ComparePasswords(userId, txtPass.Text, 1) ||
                         ComparePasswords(userId, txtPass.Text, 2) ||
                         ComparePasswords(userId, txtPass.Text, 3))
                {
                    ((Label)Master.FindControl("lblError")).Text = string.Format("Uw nieuwe wachtwoord mag niet exact hetzelfde zijn als de voorgaande drie wachtwoorden.");
                    return;
                }
            }
            setUserData(userId);

        }

        private string[] getUserData(string userId)
        {
            string[] userData = new string[8];
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT email, firstname, lastname, birthday, adres, zipcode, city, inhollandMiles FROM users WHERE userId = @Id", conn);
                cmd.Parameters.AddWithValue("@Id", userId);

                reader = cmd.ExecuteReader();
                reader.Read();

                for (int i = 0; i < userData.Length; i++)
                {
                    if (!reader.IsDBNull(i)) userData[i] = reader.GetString(i);
                }
                return userData;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private void setUserData(string userId)
        {
            string query;
            bool success = false;
            byte[] pass0 = null, pass1 = null, pass2 = null, salt0 = null, salt1 = null, salt2 = null;

            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                query = "SELECT password, passwordOld1, passwordOld2, salt, saltOld1, saltOld2 FROM users WHERE userId = @Id";
                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Id", userId);

                reader = cmd.ExecuteReader();
                reader.Read();

                if (!txtPass.Text.Equals(""))   //if a new password is entered, create a new salt and shift the database fields
                {
                    salt0 = GenerateNewSalt();
                    pass0 = HashAndSaltPass(txtPass.Text, salt0);

                    pass1 = (byte[])reader["password"];
                    if (!reader.IsDBNull(1)) pass2 = (byte[])reader["passwordOld1"];
                    salt1 = (byte[])reader["salt"];
                    if (!reader.IsDBNull(4)) salt2 = (byte[])reader["saltOld1"];
                }
                else
                {
                    pass0 = (byte[])reader["password"];
                    if(!reader.IsDBNull(1)) pass1 = (byte[])reader["passwordOld1"];
                    if (!reader.IsDBNull(2)) pass2 = (byte[])reader["passwordOld2"];
                    salt0 = (byte[])reader["salt"];
                    if (!reader.IsDBNull(4)) salt1 = (byte[])reader["saltOld1"];
                    if (!reader.IsDBNull(5)) salt2 = (byte[])reader["saltOld2"];
                }
                reader.Close();

                query = "UPDATE users SET email = @Email, password = @Pass0, passwordOld1 = @Pass1, passwordOld2 = @Pass2, salt = @Salt0, saltOld1 = @Salt1, saltOld2 = @Salt2, "
                                        + "firstname = @Firstname, lastname = @Lastname, birthday = @Birthday, adres = @Address, zipcode = @Zipcode, city = @City WHERE userId = @UserId;";
                cmd = new MySqlCommand(query, conn);
                #region queryparameters
                if (!txtEmail.Text.Equals("")) cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                else cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = DBNull.Value;

                if (pass0 != null) cmd.Parameters.AddWithValue("@Pass0", pass0);
                else cmd.Parameters.Add("@Pass0", SqlDbType.Binary).Value = DBNull.Value;

                if (pass1 != null) cmd.Parameters.AddWithValue("@Pass1", pass1);
                else cmd.Parameters.Add("@Pass1", SqlDbType.Binary).Value = DBNull.Value;

                if (pass2 != null) cmd.Parameters.AddWithValue("@Pass2", pass2);
                else cmd.Parameters.Add("@Pass2", SqlDbType.Binary).Value = DBNull.Value;

                if (salt0 != null) cmd.Parameters.AddWithValue("@Salt0", salt0);
                else cmd.Parameters.Add("@Salt0", SqlDbType.Binary).Value = DBNull.Value;

                if (salt1 != null) cmd.Parameters.AddWithValue("@Salt1", salt1);
                else cmd.Parameters.Add("@Salt1", SqlDbType.Binary).Value = DBNull.Value;

                if (salt2 != null) cmd.Parameters.AddWithValue("@Salt2", salt2);
                else cmd.Parameters.Add("@Salt2", SqlDbType.Binary).Value = DBNull.Value;

                if (!txtFirstname.Text.Equals("")) cmd.Parameters.AddWithValue("@Firstname", txtFirstname.Text);
                else cmd.Parameters.Add("@Firstname", SqlDbType.VarChar).Value = DBNull.Value;

                if (!txtLastname.Text.Equals("")) cmd.Parameters.AddWithValue("@Lastname", txtLastname.Text);
                else cmd.Parameters.Add("@Lastname", SqlDbType.VarChar).Value = DBNull.Value;
                
                DateTime birthday;
                if (!txtDate.Text.Equals("") && DateTime.TryParse(txtDate.Text, out birthday))
                    cmd.Parameters.AddWithValue("@Birthday", birthday);    //MySQL uses the "yyyy-mm-dd" format
                else cmd.Parameters.Add("@Birthday", SqlDbType.Date).Value = DBNull.Value;

                if (!txtAddress.Text.Equals("")) cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                else cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = DBNull.Value;

                if (!txtZipcode.Text.Equals("")) cmd.Parameters.AddWithValue("@Zipcode", txtZipcode.Text);
                else cmd.Parameters.Add("@Zipcode", SqlDbType.VarChar).Value = DBNull.Value;

                if (!txtCity.Text.Equals("")) cmd.Parameters.AddWithValue("@City", txtCity.Text);
                else cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = DBNull.Value;

                cmd.Parameters.AddWithValue("@UserId", userId);
                #endregion

                cmd.ExecuteReader();
                success = true;
            }
            catch (Exception ex)
            {
                ((Label)Master.FindControl("lblError")).Text = string.Format("Error: \n{0}", ex);
                return;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
                if (success) Response.Redirect("account.aspx");
            }
        }

        private bool ComparePasswords(string userId, string passText, int type)    //type specifies if it's the current (1), previous (2) or pre-previous password (3)
        {
            byte[] salt = getSalt(userId, type), pass;
            string passType, query;

            if (salt == null) return false; //no salt exists, so there is no password in the database
            pass = HashAndSaltPass(passText, salt);

            if (type == 1) passType = "password";
            else if (type == 2) passType = "passwordOld1";
            else passType = "passwordOld2";

            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                query = string.Format("SELECT {0} FROM users WHERE userId = @Id;", passType);
                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Id", userId);

                reader = cmd.ExecuteReader();
                reader.Read();

                byte[] dbPass = (byte[])reader[passType];
                if (CompareByteArrays(TrimByteArray(dbPass), pass)) return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private byte[] TrimByteArray(byte[] ba) //removes trailing zeros
        {
            bool end = false;
            int i = 63;
            while (!end && i > 0)
            {
                if (ba[i] != 0) end = true;
                i--;
            }
            byte[] baTrimmed = new byte[i + 2];
            for (i = 0; i < baTrimmed.Length; i++)
            {
                baTrimmed[i] = ba[i];
            }

            return baTrimmed;
        }
        private bool CompareByteArrays(byte[] baDb, byte[] baLoc)
        {
            if (baDb.Length == baLoc.Length)
            {
                int i = 0;
                while (i < baDb.Length && (baDb[i] == baLoc[i])) i++;
                if (i == baDb.Length) return true;
            }

            return false;
        }

        private byte[] getSalt(string userId, int type)
        {
            string saltType, query;
            if (type == 1) saltType = "salt";
            else if (type == 2) saltType = "saltOld1";
            else saltType = "saltOld2";

            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                query = string.Format("SELECT {0} FROM users WHERE userId = @Id;", saltType);
                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Id", userId);

                reader = cmd.ExecuteReader();
                reader.Read();
                //if (reader.IsDBNull(0)) return GenerateNewSalt();
                return (byte[])reader[saltType];
            }
            catch (Exception ex)
            {
                return null;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private byte[] HashAndSaltPass(string plainText, byte[] salt)
        {
            //source: http://www.obviex.com/samples/hash.aspx
            // Generates a hash for the given plain text value. Before the
            // hash is computed, a random salt is generated and appended
            // to the plain text. This salt is stored at the end of the 
            // hash value, so it can be used later for hash verification.

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes = new byte[plainTextBytes.Length + salt.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < salt.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = salt[i];

            // Create the hashing alhorithm
            SHA256 hash = new SHA256Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length + salt.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < salt.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = salt[i];

            // Return the result.
            return hashWithSaltBytes;
        }

        private byte[] GenerateNewSalt()
        {
            //Generate 128bit salt
            RNGCryptoServiceProvider crng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[16];
            crng.GetNonZeroBytes(salt);

            return salt;
        }
    }
}