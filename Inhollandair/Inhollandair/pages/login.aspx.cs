﻿using MySql.Data.MySqlClient;
using System;

using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Inhollandair.pages
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string text = (Guid.NewGuid().ToString()).Substring(0, 5);
            Response.Cookies["Captcha"].Value = text;
            imgCaptcha.ImageUrl = "~/captcha.aspx";

            if (!IsPostBack)
            {
                try
                {
                    ViewState["PreviousPageUrl"] = Request.UrlReferrer.ToString();  //store previous page url (including querystring) in a viewstate
                }
                catch (Exception ex)
                {
                    Response.Redirect("home.aspx");
                }
            }
        }

        protected void btn_Login_Click(object sender, EventArgs e)
        {
            if (txtCaptcha.Text != Request.Cookies["Captcha"].Value)
                ((Label)Master.FindControl("lblError")).Text = "Captcha incorrect.";
            else
            {
                if (Login())
                {
                    CreateUserCookie();
                    Response.Redirect(ViewState["PreviousPageUrl"].ToString());
                }
                else ((Label)Master.FindControl("lblError")).Text = "Gebruikersnaam of wachtwoord fout.";
            }
        }

        protected void lnkCaptcha_Click(object sender, EventArgs e)
        {
            Response.Cookies["Captcha"].Value = (Guid.NewGuid().ToString()).Substring(0, 5);
            imgCaptcha.ImageUrl = "~/captcha.aspx";
        }

        private bool Login()
        {
            byte[] salt = getSalt(txt_Email.Text), pass = HashAndSaltPass(txt_Pass.Text, salt);

            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT email, password FROM users WHERE email = @Email;", conn);
                cmd.Parameters.AddWithValue("@Email", txt_Email.Text);

                reader = cmd.ExecuteReader();
                reader.Read();

                byte[] dbPass = (byte[])reader["password"];
                if (reader.GetString(0) == txt_Email.Text && CompareByteArrays(TrimByteArray(dbPass), pass)) return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private byte[] TrimByteArray(byte[] ba) //removes trailing zeros
        {
            bool end = false;
            int i = 63;
            while (!end && i > 0)
            {
                if (ba[i] != 0) end = true;
                i--;
            }
            byte[] baTrimmed = new byte[i + 2];
            for (i = 0; i < baTrimmed.Length; i++)
            {
                baTrimmed[i] = ba[i];
            }

            return baTrimmed;
        }
        private bool CompareByteArrays(byte[] baDb, byte[] baLoc)
        {
            if (baDb.Length == baLoc.Length)
            {
                int i = 0;
                while (i < baDb.Length && (baDb[i] == baLoc[i])) i++;
                if (i == baDb.Length) return true;
            }

            return false;
        }

        private byte[] getSalt(string email)
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT salt FROM users WHERE email = @Email;", conn);
                cmd.Parameters.AddWithValue("@Email", email);

                reader = cmd.ExecuteReader();
                reader.Read();
                return (byte[])reader["salt"];
            }
            catch (Exception ex)
            {
                return null;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private byte[] HashAndSaltPass(string plainText, byte[] salt)
        {
            //source: http://www.obviex.com/samples/hash.aspx
            // Generates a hash for the given plain text value. Before the
            // hash is computed, a random salt is generated and appended
            // to the plain text. This salt is stored at the end of the 
            // hash value, so it can be used later for hash verification.

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes = new byte[plainTextBytes.Length + salt.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < salt.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = salt[i];

            // Create the hashing alhorithm
            SHA256 hash = new SHA256Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length + salt.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < salt.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = salt[i];

            // Return the result.
            return hashWithSaltBytes;
        }

        private string getDbUserId()
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            conn.Open();

            try
            {
                MySqlCommand cmd = new MySqlCommand("SELECT userId FROM users WHERE email = @Email;", conn);
                cmd.Parameters.AddWithValue("@Email", txt_Email.Text);

                reader = cmd.ExecuteReader();
                reader.Read();
                return reader.GetString(0);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        private void CreateUserCookie()
        {
            HttpCookie userCookie = new HttpCookie("userCookie");
            userCookie.Value = getDbUserId();
            userCookie.Expires = DateTime.Now.AddYears(1);          //cookie stays alive for one year
            Response.Cookies.Add(userCookie);
        }
    }
}