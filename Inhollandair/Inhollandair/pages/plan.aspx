﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="plan.aspx.cs" Inherits="Inhollandair.pages.products" %>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    Inhollandair - Plannen
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <table style="display:none;"> <!-- not working-->
        <tr>
            <td><b>Filters:</b></td>
        </tr>
        <tr>
            <td>Datum:</td>
            <td>
                <asp:CheckBox ID="chkDate" runat="server" />
                <asp:TextBox runat="server" ID="txtDate" AutoPostBack="True" OnTextChanged="Filter_Checked" />
                <asp:RegularExpressionValidator ID="revDate" runat="server" ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="Ongeldige Datum. (Format is dd-mm-yyyy)"  ValidationGroup="valFilter" ValidationExpression="^(?:(?:31(-)(?:0[13578]|1[02]))\1|(?:(?:29|30)(-)(?:0[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(-)02\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0[1-9]|1\d|2[0-8])(-)(?:(?:0[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)\d{2})$" />
            </td>
        </tr>
        <tr>
            <td>Tijd:</td>
            <td>
                <asp:CheckBox ID="chkTime" runat="server" />
                <asp:TextBox runat="server" ID="txtTime" AutoPostBack="True" OnTextChanged="Filter_Checked" />
                <asp:RegularExpressionValidator ID="revTime" runat="server" ControlToValidate="txtTime" Display="Dynamic" ErrorMessage="Ongeldige tijd. (Format is HH:MM)" ValidationGroup="valFilter" ValidationExpression="^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$" />
            </td>
        </tr>
        <tr>
            <td>Vanaf:</td>
            <td>
                <asp:CheckBox ID="chkOrigin" runat="server" />
                <asp:DropDownList ID="ddlOrigin" runat="server" DataSourceID="mySqlDataSourceOrigin" DataTextField="origin" DataValueField="origin" AutoPostBack="True" OnSelectedIndexChanged="Filter_Checked" />
            </td>
        </tr>
        <tr>
            <td>Bestemming:</td>
            <td>
                <asp:CheckBox ID="chkDestination" runat="server" />
                <asp:DropDownList ID="ddlDestination" runat="server" DataSourceID="mySqlDataSourceOrigin" DataTextField="origin" DataValueField="origin" AutoPostBack="True" OnSelectedIndexChanged="Filter_Checked" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button runat="server" ID="btnFilter" Text="Filter" ValidationGroup="valFilter" OnClick="btnFilter_Click"/>
            </td>
        </tr>
    </table>
    <asp:GridView ID="gridPlan" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="MySqlDataSource" EnableModelValidation="True" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="productTable" OnRowCommand="gridPlan_RowCommand" DataKeyNames="flightId">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:ButtonField Text="order" ButtonType="Button" CommandName="Order">
                <ItemStyle Wrap="False" />
            </asp:ButtonField>
            <asp:BoundField DataField="date" HeaderText="datum" SortExpression="date">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="origin" HeaderText="vertrekpunt" SortExpression="origin">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="destination" HeaderText="bestemming" SortExpression="destination">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="price" HeaderText="prijs" SortExpression="price">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="inhollandMiles" HeaderText="inhollandMiles" SortExpression="inhollandMiles" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <HeaderStyle BackColor="#EC008C" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#EC008C" ForeColor="White" HorizontalAlign="Right" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:GridView>


    <asp:SqlDataSource ID="MySqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:MySqlConnectionString %>" ProviderName="<%$ ConnectionStrings:MySqlConnectionString.ProviderName %>" SelectCommand="SELECT flightId, `date`, price, origin, destination, inhollandMiles FROM flights WHERE status = 'actief' AND date >= NOW();"></asp:SqlDataSource>
    <asp:SqlDataSource ID="mySqlDataSourceOrigin" runat="server" ConnectionString="<%$ ConnectionStrings:MySqlConnectionString %>" ProviderName="<%$ ConnectionStrings:MySqlConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT origin FROM flights"></asp:SqlDataSource>
    <asp:SqlDataSource ID="mySqlDataSourceDestination" runat="server" ConnectionString="<%$ ConnectionStrings:MySqlConnectionString %>" ProviderName="<%$ ConnectionStrings:MySqlConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT destination FROM flights"></asp:SqlDataSource>
</asp:Content>
