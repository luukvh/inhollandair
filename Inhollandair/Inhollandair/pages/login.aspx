﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Inhollandair.pages.login" %>

<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    Inhollandair - Login
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <table>
        <tr>
            <td><b>Login</b></td>
        </tr>
        <tr>
            <td>Email adres:</td>
            <td>
                <asp:TextBox ID="txt_Email" runat="server" />
                <asp:RequiredFieldValidator ID="Email" runat="server" ControlToValidate="txt_Email" ValidationGroup="LoginWizard">Verplicht veld</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Wachtwoord:</td>
            <td>
                <asp:TextBox ID="txt_Pass" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="Pass" runat="server" ControlToValidate="txt_Pass" ValidationGroup="LoginWizard">Verplicht veld</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Vul de tekst hieronder in:
            </td>
            <td>
                <asp:TextBox ID="txtCaptcha" runat="server" />
                <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtCaptcha" ValidationGroup="LoginWizard">Verplicht veld</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:image ID="imgCaptcha" runat="server" imgeurl="~/captcha.aspx" />
            </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkCaptcha" OnClick="lnkCaptcha_Click" Text="Vernieuwen" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_Login" runat="server" Text="Login" ValidationGroup="LoginWizard" OnClick="btn_Login_Click" CausesValidation="true"/> 
            </td>
        </tr>
    </table>
    <br />

    <a href="register.aspx">Registreren</a> | <a href="passwordRecovery.aspx">Wachtwoord vergeten</a>
</asp:Content>
