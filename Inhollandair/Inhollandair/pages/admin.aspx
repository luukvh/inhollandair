﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="Inhollandair.pages.admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">Inhollandair - InhollandMiles</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <b>InhollandMiles aanpassen:</b><br />
    <asp:DropDownList runat="server" ID="ddlEmail" AutoPostBack="True" DataSourceID="MySqlDataSourceEmails" DataTextField="email" DataValueField="email" OnSelectedIndexChanged="ddlEmail_SelectedIndexChanged" OnDataBound="ddlEmail_DataBound" />
    <asp:SqlDataSource ID="MySqlDataSourceEmails" runat="server" ConnectionString="<%$ ConnectionStrings:MySqlConnectionString %>" ProviderName="<%$ ConnectionStrings:MySqlConnectionString.ProviderName %>" SelectCommand="SELECT email FROM users;"></asp:SqlDataSource>
    <asp:TextBox runat="server" ID="txtNewMiles" />&nbsp;
    <asp:Button runat="server" ID="btnUpdateMiles" Text="Update" OnClick="btnUpdateMiles_Click" />
    <br />
    <br />
    <b>Nieuwe vlucht toevoegen:</b>
    <table>
        <tr>
            <td>Datum:</td>
            <td>
                <asp:TextBox runat="server" ID="txtDate" />
                <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate" ValidationGroup="vgAddFlight" ErrorMessage="Verplicht veld."></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td>Prijs:</td>
            <td>
                <asp:TextBox runat="server" ID="txtPrice" />
                <asp:RequiredFieldValidator ID="rfvPrice" runat="server" ControlToValidate="txtDate" ValidationGroup="vgAddFlight" ErrorMessage="Verplicht veld."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Vanaf:</td>
            <td>
                <asp:TextBox runat="server" ID="txtOrigin" />
                <asp:RequiredFieldValidator ID="rfvOrigin" runat="server" ControlToValidate="txtDate" ValidationGroup="vgAddFlight" ErrorMessage="Verplicht veld."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Bestemming:</td>
            <td>
                <asp:TextBox runat="server" ID="txtDestination" />
                <asp:RequiredFieldValidator ID="rfvDestination" runat="server" ControlToValidate="txtDate" ValidationGroup="vgAddFlight" ErrorMessage="Verplicht veld."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>InhollandMiles:</td>
            <td>
                <asp:TextBox runat="server" ID="txtMiles" />
                <asp:RequiredFieldValidator ID="rfvMiles" runat="server" ControlToValidate="txtDate" ValidationGroup="vgAddFlight" ErrorMessage="Verplicht veld."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="btnAdd" Text="Toevoegen" ValidationGroup="vgAddFlight" OnClick="btnAdd_Click"/>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <b>Vlucht annuleren:</b><br />
    <asp:GridView ID="gridPlan" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="MySqlDataSourceFlights" EnableModelValidation="True" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="productTable" OnRowCommand="gridPlan_RowCommand" DataKeyNames="flightId">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:ButtonField Text="Annuleren" ButtonType="Button" CommandName="Cancel">
                <ItemStyle Wrap="False" />
            </asp:ButtonField>
            <asp:BoundField DataField="status" HeaderText="status" SortExpression="status" />
            <asp:BoundField DataField="date" HeaderText="datum" SortExpression="date">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="origin" HeaderText="vertrekpunt" SortExpression="origin">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="destination" HeaderText="bestemming" SortExpression="destination">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="price" HeaderText="prijs" SortExpression="price">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="inhollandMiles" HeaderText="inhollandMiles" SortExpression="inhollandMiles" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#EC008C" />
        <HeaderStyle BackColor="#EC008C" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#EC008C" ForeColor="White" HorizontalAlign="Right" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:GridView>
    <asp:SqlDataSource ID="MySqlDataSourceFlights" runat="server" ConnectionString="<%$ ConnectionStrings:MySqlConnectionString %>" ProviderName="<%$ ConnectionStrings:MySqlConnectionString.ProviderName %>" SelectCommand="SELECT flightId, status, `date`, price, origin, destination, inhollandMiles FROM flights WHERE status = 'actief';"></asp:SqlDataSource>
    <br />
    <br />
</asp:Content>
