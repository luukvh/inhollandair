﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;

namespace Inhollandair.pages
{
    public partial class admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["userCookie"] == null)
                    Response.Redirect("login.aspx");
                else if (!CheckIsAdmin(Request.Cookies["userCookie"].Value))
                    Response.Redirect("home.aspx");
            }
        }

        public bool CheckIsAdmin(string userId)   //check if the user is an admin, otherwise redirect
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT role FROM users WHERE userId = @Id;", conn);
                cmd.Parameters.AddWithValue("@Id", userId);

                reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.GetString(0) == "admin") return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        protected void ddlEmail_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT inhollandMiles FROM users WHERE email = @Email;", conn);
                cmd.Parameters.AddWithValue("@Email", ddlEmail.SelectedValue);

                reader = cmd.ExecuteReader();
                reader.Read();
                txtNewMiles.Text = reader.GetString(0);
            }
            catch (Exception ex)
            {
                ((Label)Master.FindControl("lblError")).Text = ex.ToString();
                return;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
        }

        protected void btnUpdateMiles_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("UPDATE users SET inhollandMiles = @Miles WHERE email = @Email;", conn);
                cmd.Parameters.AddWithValue("@Miles", txtNewMiles.Text);
                cmd.Parameters.AddWithValue("@Email", ddlEmail.SelectedValue);

                cmd.ExecuteReader();
                ((Label)Master.FindControl("lblError")).Text = "InhollandMiles succesvol aangepast.";
            }
            catch (Exception ex)
            {
                return;
            }
            finally //close all open connections
            {
                //no reader
                if (conn != null) conn.Close();
            }
            Response.AddHeader("REFRESH", "1;URL=admin.aspx"); ;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("INSERT INTO flights (`date`, `price`, `origin`, `destination`, `inhollandMiles`, `status`) VALUES (@Date, @Price, @Origin, @Destination, @Miles, 'actief');", conn);
                DateTime birthday;
                if (DateTime.TryParse(txtDate.Text, out birthday))
                    cmd.Parameters.AddWithValue("@Date", birthday);    //MySQL uses the "yyyy-mm-dd" format
                else cmd.Parameters.Add("@Birthday", SqlDbType.Date).Value = DBNull.Value;
                cmd.Parameters.AddWithValue("@Price", txtPrice.Text);
                cmd.Parameters.AddWithValue("@Origin", txtOrigin.Text);
                cmd.Parameters.AddWithValue("@Destination", txtDestination.Text);
                cmd.Parameters.AddWithValue("@Miles", txtMiles.Text);

                cmd.ExecuteReader();
                ((Label)Master.FindControl("lblError")).Text = "InhollandMiles succesvol aangepast.";
            }
            catch (Exception ex)
            {
                ((Label)Master.FindControl("lblError")).Text = ex.ToString();
                return;
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
            Response.AddHeader("REFRESH", "1;URL=admin.aspx"); ;
        }

        protected void gridPlan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                int index = Convert.ToInt32(e.CommandArgument);             //This will break when the table index is larger than 2.147.483.647 (int overflow)
                string dbId = gridPlan.DataKeys[index].Value.ToString();

                CancelFlight(dbId, "geannuleerd");                          //maybe add possibility for admins to select if a flight was cancelled or expired
            }
        }

        private void CancelFlight(string dbId, string status)
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("UPDATE flights SET status = @Status WHERE flightId = @Id;", conn);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@Id", dbId);

                cmd.ExecuteReader();
                ((Label)Master.FindControl("lblError")).Text = "Vlucht succesvol geannuleerd.";
            }
            catch (Exception ex)
            {
                ((Label)Master.FindControl("lblError")).Text = ex.ToString();
                return;
            }
            finally //close all open connections
            {
                //no reader
                if (conn != null) conn.Close();
            }
            Response.AddHeader("REFRESH", "1;URL=admin.aspx"); ;
        }

        protected void ddlEmail_DataBound(object sender, EventArgs e)
        {
            ddlEmail_SelectedIndexChanged(ddlEmail, e);
        }
    }
}