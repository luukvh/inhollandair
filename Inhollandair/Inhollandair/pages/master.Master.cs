﻿using System;
using System.Web;
using System.Web.Security;

namespace Inhollandair.pages
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        #region XSRF/CSRF protection
        //XSRF/CSRF protection: http://stackoverflow.com/a/29940861/2099393
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && GuidTryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        private bool GuidTryParse(string cookieValue, out Guid requestCookieGuidValue)  //tryparse replacement
        {
            requestCookieGuidValue = Guid.Empty;

            try
            {
                requestCookieGuidValue = new Guid(cookieValue);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }

            catch (OverflowException)
            {
                return false;
            }
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies["userCookie"];
            if (cookie != null) lnkLogin.Text = "Loguit";       //check if there is a user logged in

            /*cookie = Request.Cookies["cartCookie"];
            if (cookie != null)                                 //check if there are items in the cart, display how many items there are in the cart
            {
                string[] flightIdArray = cookie.Value.Split(',');
                lnkCart.Text = string.Format("Winkelwagen ({0})", flightIdArray.Length);
            }
            else lnkCart.Text = "Winkelwagen";
            */
        }

        protected void lnkLogin_Click(object sender, EventArgs e)
        {
            HttpCookie userCookie = Request.Cookies["userCookie"];
            if (userCookie != null)
            {
                HttpContext.Current.Response.Cookies.Remove("userCookie");
                userCookie.Expires = DateTime.Now.AddDays(-10);
                userCookie.Value = null;
                HttpContext.Current.Response.SetCookie(userCookie);

                Response.Redirect(Request.RawUrl);
            }
            else Response.Redirect("login.aspx");
        }
    }
}