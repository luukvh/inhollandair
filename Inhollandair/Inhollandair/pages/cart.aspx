﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="cart.aspx.cs" Inherits="Inhollandair.pages.cart" %>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    Inhollandair - Bestellen
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <asp:GridView ID="gridCart" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="productTable" EnableModelValidation="True" RowStyle-HorizontalAlign="Center">
        <Columns>
            <asp:TemplateField HeaderText="Aantal">
                <ItemTemplate>
                    <asp:TextBox ID="TxtAmount" runat="server" Width="2em" AutoPostBack="True" MaxLength="2" OnTextChanged="txtAmount_Changed" Text="1" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <HeaderStyle BackColor="#EC008C" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#EC008C" ForeColor="White" HorizontalAlign="Right" />
    </asp:GridView>
     
    <br />
    <br />
    <table>
        <tr>
            <td>Prijs (Euros): </td>
            <td class="right"><asp:Label runat="server" ID="lblPrice" /></td>
            <td><asp:Button runat="server" ID="btnOrder" Text="Bestel" OnClick="btnOrder_Click" /></td>
        </tr>
        <tr>
            <td>Prijs (InhollandMiles): </td>
            <td class="right">&nbsp;<asp:Label runat="server" ID="lblPriceMiles" /></td>
            <td><asp:Button runat="server" ID="btnOrderMiles" Text="Bestel met InhollandMiles" OnClick="btnOrderMiles_Click" /></td>
        </tr>
    </table>
</asp:Content>
