﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace Inhollandair.pages
{
    public partial class products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gridPlan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Order")
            {
                Int32 index = Convert.ToInt32(e.CommandArgument);             //This will break when the table index is larger than 2.147.483.647 (int overflow)
                string dbId = gridPlan.DataKeys[index].Value.ToString();
                
                Response.Redirect("cart.aspx?product=" + dbId);
            }
        }

        protected void Filter_Checked(object sender, EventArgs e)
        {
            if (sender == txtDate && txtDate.Text != "")
                chkDate.Checked = true;
            else if (sender == txtTime && txtTime.Text != "")
                chkTime.Checked = true;
            else if (sender == ddlOrigin)
                chkOrigin.Checked = true;
            else if (sender == ddlDestination)
                chkDestination.Checked = true;
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            string origin = null, destination = null;
            DateTime time = new DateTime();

            if (chkOrigin.Checked)
                origin = ddlOrigin.SelectedValue;
            if (chkDestination.Checked)
                destination = ddlDestination.SelectedValue;

            if (chkDate.Checked && txtDate.Text != "")
            {
                string[] date = txtDate.Text.Split('-');
                string dateTime = string.Format("{0}-{1}-{2}", date[2], date[1], date[0]);

                if (chkTime.Checked && txtTime.Text != "")
                    time = DateTime.ParseExact(dateTime + " " + txtTime.Text, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                else
                    time = DateTime.ParseExact(dateTime, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else if (chkTime.Checked && txtTime.Text != "")
                time = DateTime.ParseExact(txtTime.Text, "HH:mm", CultureInfo.InvariantCulture);

            Filter(time, origin, destination);
        }

        private void Filter(DateTime time, string origin, string destination)
        {
            //filter flights            
        }
    }
}