﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="orderHistory.aspx.cs" Inherits="Inhollandair.pages.orderHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">Inhollandair - Bestellingen
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <asp:GridView ID="gridHistory" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="productTable" RowStyle-HorizontalAlign="Center" EnableModelValidation="True" AllowPaging="True" AlternatingRowStyle-BackColor="White">
        <EditRowStyle BackColor="#2461BF" />
        <HeaderStyle BackColor="#EC008C" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#EC008C" ForeColor="White" HorizontalAlign="Right" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:GridView>
</asp:Content>
