﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Inhollandair.pages.register" %>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    Inhollandair - Registreren
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <table>
        <tr>
            <td><b>Registreren</b></td>
        </tr>
        <tr>
            <td>*Email adres:</td>
            <td>
                <asp:TextBox ID="txt_Email" runat="server" />
                <asp:RequiredFieldValidator ID="Email" runat="server" ControlToValidate="txt_Email" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ErrorMessage="Incorrect emailaddress" ValidationExpression="^[^@]+@[^@]+\.[^@]+$" ControlToValidate="txt_Email" Display="Dynamic" /> <!-- only one @, at least one period after @ and one character before, between and after @ and period. -->
            </td>
        </tr>
        <tr>
            <td>*Wachtwoord:</td>
            <td>
                <asp:TextBox ID="txt_Pass" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="Pass" runat="server" ControlToValidate="txt_Pass" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="PassValidator" runat="server" ErrorMessage="Wachtwoord moet bestaan uit één kleine letter, hoofdletter, cijfer en vreemd teken (geen spaties) en minimaal tien tekens lang zijn." ValidationExpression="^(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[0-9])(?=\S*[@#$%^&+=])\S*$" ControlToValidate="txt_Pass" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td>*Wachtwoord nogmaals:</td>
            <td>
                <asp:TextBox ID="txt_PassAgain" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="PassAgain" runat="server" ControlToValidate="txt_PassAgain" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator" runat="server" ControlToValidate="txt_PassAgain" ErrorMessage="Incorrect wachtwoord" ControlToCompare="txt_Pass" Display="Dynamic" />
            </td>
        </tr>

        <tr>
            <td>*Voornaam:</td>
            <td>
                <asp:TextBox ID="txt_Name" runat="server" />
                <asp:RequiredFieldValidator ID="Name" runat="server" ControlToValidate="txt_Name" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>*Achternaam:</td>
            <td>
                <asp:TextBox ID="txt_Surname" runat="server" />
                <asp:RequiredFieldValidator ID="Surname" runat="server" ControlToValidate="txt_Surname" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Geboortedatum:</td>
            <td>
                <asp:TextBox ID="Txt_Date" runat="server" />
                <asp:RegularExpressionValidator ID="DateValidator" runat="server" ErrorMessage="Incorrect birthday (DD-MM-YYYY)" ValidationExpression="^(?:(?:31(-)(?:0[13578]|1[02]))\1|(?:(?:29|30)(-)(?:0[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(-)02\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0[1-9]|1\d|2[0-8])(-)(?:(?:0[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)\d{2})$" ControlToValidate="txt_Date" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td>Adres:</td>
            <td>
                <asp:TextBox ID="txt_Adres" runat="server" />
                <asp:RegularExpressionValidator ID="AdresValidator" runat="server" ErrorMessage="Incorrect adres" ValidationExpression="^[A-z]+\s[0-9]+[a-z]?$" ControlToValidate="txt_Adres" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td>Postcode:</td>
            <td>
                <asp:TextBox ID="txt_ZipCode" runat="server" />
                <asp:RegularExpressionValidator ID="ZipCodeValidator" runat="server" ErrorMessage="Incorrect zipcode" ValidationExpression="^[1-9][0-9]{3}\s?[A-Z]{2}$" ControlToValidate="txt_ZipCode" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td>Woonplaats:</td>
            <td>
                <asp:TextBox ID="txt_City" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Button ID="btn_Add" runat="server" Text="Maak aan" ValidationGroup="CreateUserWizard1" OnClick="btn_Add_Click" /><br />
    <small>*Verplichte velden</small>
</asp:Content>
