﻿using System;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace Inhollandair.pages
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Add_Click(object sender, EventArgs e)
        {
            if (!UniqueEmail()) ((Label)Master.FindControl("lblError")).Text = "Email adres bestaat al!";
            else
            {
                byte[] salt, pass = HashAndSaltNewPass(txt_Pass.Text, out salt);

                MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
                MySqlDataReader reader = null;
                try
                {
                    conn.Open();

                    Guid guid = getUnusedGuid(conn);
                    string query = "INSERT INTO users (`userId`, `email`, `password`, `salt`, `firstname`, `lastname`, `birthday`, `adres`, `zipcode`, `city`, `inhollandMiles`, `role`)" +
                                              "VALUES (@UserId , @Email , @Password , @Salt , @Firstname , @Lastname , @Birthday , @Adres , @Zipcode , @City , '0', 'user');";

                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@userId", guid.ToString());
                    cmd.Parameters.AddWithValue("@Email", txt_Email.Text);
                    cmd.Parameters.AddWithValue("@Password", pass);
                    cmd.Parameters.AddWithValue("@Salt", salt);
                    cmd.Parameters.AddWithValue("@Firstname", txt_Name.Text);
                    cmd.Parameters.AddWithValue("@Lastname", txt_Surname.Text);
                    cmd.Parameters.AddWithValue("@Adres", txt_Adres.Text);
                    cmd.Parameters.AddWithValue("@Zipcode", txt_ZipCode.Text);
                    cmd.Parameters.AddWithValue("@City", txt_City.Text);
                    DateTime birthday;
                    if (DateTime.TryParse(Txt_Date.Text, out birthday))
                        cmd.Parameters.AddWithValue("@Birthday", birthday);    //MySQL uses the "yyyy-mm-dd" format
                    else cmd.Parameters.Add("@Birthday", SqlDbType.Date).Value = DBNull.Value;


                    reader = cmd.ExecuteReader();
                    ((Label)Master.FindControl("lblError")).Text = "Gebruiker is succesvol aangemaakt. U wordt over een paar seconde teruggebracht naar de home pagina.";
                    Response.AddHeader("REFRESH", "3;URL=login.aspx");
                }
                catch (Exception ex)
                {
                    ((Label)Master.FindControl("lblError")).Text = ex.ToString();
                }
                finally //close all open connections
                {
                    if (reader != null) reader.Close();
                    if (conn != null) conn.Close();
                }
            }
        }

        private bool UniqueEmail()
        {
            bool unique = false;

            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString);
            MySqlDataReader reader = null;
            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT email FROM users WHERE email = @Email;", conn);
                cmd.Parameters.AddWithValue("@Email", txt_Email.Text);
                reader = cmd.ExecuteReader();

                if (!reader.HasRows) unique = true; //no rows available, so the email has not been found in the database.
            }
            catch (Exception ex)
            {
                ((Label)Master.FindControl("lblError")).Text = ex.ToString();
            }
            finally //close all open connections
            {
                if (reader != null) reader.Close();
                if (conn != null) conn.Close();
            }
            return unique;
        }

        private byte[] HashAndSaltNewPass(string plainText, out byte[] salt)
        {
            //source: http://www.obviex.com/samples/hash.aspx
            // Generates a hash for the given plain text value. Before the
            // hash is computed, a random salt is generated and appended
            // to the plain text. This salt is stored at the end of the 
            // hash value, so it can be used later for hash verification.

            //Generate 128bit salt
            RNGCryptoServiceProvider crng = new RNGCryptoServiceProvider();
            salt = new byte[16];
            crng.GetNonZeroBytes(salt);

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes = new byte[plainTextBytes.Length + salt.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < salt.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = salt[i];

            // Create the hashing alhorithm
            SHA256 hash = new SHA256Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length + salt.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < salt.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = salt[i];

            // Return the result.
            return hashWithSaltBytes;
        }

        private Guid getUnusedGuid(MySqlConnection conn)
        {
            bool used = true;
            Guid guid = Guid.NewGuid();
            string query = "SELECT userId FROM users WHERE userId  = @UserId;";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader reader;

            while (used)
            {
                cmd.Parameters.AddWithValue("@UserId", guid);
                reader = cmd.ExecuteReader();

                if (!reader.HasRows) used = false;
                else guid = Guid.NewGuid();

                reader.Close();
            }

            return guid;
        }
    }
}