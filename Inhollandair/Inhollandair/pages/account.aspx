﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/master.Master" AutoEventWireup="true" CodeBehind="account.aspx.cs" Inherits="Inhollandair.pages.account" %>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    Inhollandair - Account
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <asp:HyperLink ID="lblHistory" runat="server" NavigateUrl="orderHistory.aspx" Text="Bestel geschiedenis" /><br />
    <asp:HyperLink ID="lblAdmin" runat="server" NavigateUrl="admin.aspx" Text="Admin pagina" visible="false"/>
    <br />
    <br />
    <asp:Table runat="server" ID="tabData">
        <asp:TableRow>
            <asp:TableCell Width="200px"><b>Gegevens:</b></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>Email-adres:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblEmail" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>Naam:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblName" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>Geboorte datum:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblBirthday" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>Adres:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblAdres" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>Postcode:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblZipcode" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>Woonplaats:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblCity" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell>InhollandMiles:</asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="lblMiles" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="30px">
            <asp:TableCell><small><a href="account.aspx?mode=edit">Gegevens bewerken</a></small></asp:TableCell>
        </asp:TableRow>
    </asp:Table>


    <asp:Table runat="server" ID="TabEdit" Visible="false">
        <asp:TableRow>
            <asp:TableCell Width="200px"><b>Gegevens bewerken:</b></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Email-adres:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtEmail" />
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ErrorMessage="Incorrect emailaddress" ValidationExpression="^[^@]+@[^@]+\.[^@]+$" ControlToValidate="txtEmail" Display="Dynamic" /> <!-- only one @, at least one period after @ and one character before, between and after @ and period. -->
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Voornaam:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtFirstname" />
                <asp:RequiredFieldValidator ID="rfvFirstname" runat="server" ControlToValidate="txtFirstname" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Achternaam:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtLastname" />
                <asp:RequiredFieldValidator ID="rfvLastname" runat="server" ControlToValidate="txtLastname" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Geboorte datum:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtDate" />
                <asp:RegularExpressionValidator ID="DateValidator" runat="server" ErrorMessage="Incorrect birthday (DD-MM-YYYY)" ValidationExpression="^(?:(?:31(-)(?:0[13578]|1[02]))\1|(?:(?:29|30)(-)(?:0[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(-)02\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0[1-9]|1\d|2[0-8])(-)(?:(?:0[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)\d{2})$" ControlToValidate="txtDate" Display="Dynamic" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Adres:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtAddress" />
                <asp:RegularExpressionValidator ID="AdresValidator" runat="server" ErrorMessage="Incorrect adres" ValidationExpression="^[A-z]+\s[0-9]+[a-z]?$" ControlToValidate="txtAddress" Display="Dynamic" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Postcode:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtZipcode" />
                <asp:RegularExpressionValidator ID="ZipCodeValidator" runat="server" ErrorMessage="Incorrect zipcode" ValidationExpression="^[1-9][0-9]{3}\s?[A-Z]{2}$" ControlToValidate="txtZipcode" Display="Dynamic" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Woonplaats:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtCity" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Nieuw wachtwoord:</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtPass" TextMode="Password" />
                <asp:RegularExpressionValidator ID="PassValidator" runat="server" ErrorMessage="Wachtwoord moet bestaan uit één kleine letter, hoofdletter, cijfer en vreemd teken (geen spaties) en minimaal tien tekens lang zijn." ValidationExpression="^(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[0-9])(?=\S*[@#$%^&+=])\S*$" ControlToValidate="txtPass" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Nieuw wachtwoord:<br />(nogmaals)</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtPassAgain" TextMode="Password" />
                <asp:CompareValidator ID="CompareValidator" runat="server" ControlToValidate="txtPassAgain" ErrorMessage="Incorrect wachtwoord" ControlToCompare="txtPass" Display="Dynamic" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="seperator" />
            <asp:TableCell CssClass="seperator" />
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Oud wachtwoord: <br />(ter bevestiging)</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtOldPass" TextMode="Password" />
                <asp:RequiredFieldValidator ID="Pass" runat="server" ControlToValidate="txtOldPass" ValidationGroup="CreateUserWizard1" Display="Dynamic">Verplicht veld</asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Button ID="btnUpdate" runat="server" Text="Wijzigen" ValidationGroup="CreateUserWizard1" OnClick="btn_Update_Click" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
