-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: inhollandair
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flights` (
  `flightId` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `origin` varchar(45) NOT NULL,
  `destination` varchar(45) NOT NULL,
  `inhollandMiles` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`flightId`),
  UNIQUE KEY `flightId_UNIQUE` (`flightId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights`
--

LOCK TABLES `flights` WRITE;
/*!40000 ALTER TABLE `flights` DISABLE KEYS */;
INSERT INTO `flights` VALUES (1,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'geannuleerd'),(3,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(4,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(5,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(6,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(7,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(8,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(9,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(10,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(11,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(13,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(14,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(15,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(16,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(17,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(18,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(19,'2016-05-03 00:00:00',50,'Amsterdam Airport Schiphol','Rotterdam',100,'actief'),(20,'2016-05-03 00:00:00',50,'Rotterdam','Amsterdam Airport Schiphol',100,'actief'),(21,'2016-01-15 00:00:00',10,'Wieringerwaard','Alkmaar',500,'verlopen'),(22,'2016-01-01 00:00:00',1,'test','test',1,'geannuleerd'),(23,'1990-01-01 00:00:00',1,'a','b',1,'geannuleerd');
/*!40000 ALTER TABLE `flights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderId` char(36) NOT NULL,
  `userId` char(36) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `flightId` int(11) NOT NULL,
  `paymentType` varchar(45) NOT NULL,
  `status` varchar(10) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `orderId_UNIQUE` (`orderId`),
  KEY `userId_idx` (`userId`),
  KEY `flightId_idx` (`flightId`),
  KEY `userId_idx1` (`paymentType`),
  CONSTRAINT `flightId` FOREIGN KEY (`flightId`) REFERENCES `flights` (`flightId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userId` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('80c0f616-545f-4fae-b312-78a71f02a37d','94b6fb73-9b11-4ba7-b6d3-c24d60b5d641',50,3,'geld','ordered',1),('cec8ca7c-9832-4fa3-af35-315c49f02ac9','94b6fb73-9b11-4ba7-b6d3-c24d60b5d641',100,21,'geld','ordered',10);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` char(36) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` binary(64) NOT NULL,
  `passwordOld1` binary(64) DEFAULT NULL,
  `passwordOld2` binary(64) DEFAULT NULL,
  `salt` binary(16) NOT NULL,
  `saltOld1` binary(16) DEFAULT NULL,
  `saltOld2` binary(16) DEFAULT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `birthday` date DEFAULT NULL,
  `adres` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `inhollandMiles` int(32) DEFAULT NULL,
  `role` varchar(10) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId_UNIQUE` (`userId`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `salt_UNIQUE` (`salt`),
  UNIQUE KEY `password_UNIQUE` (`password`),
  UNIQUE KEY `saltOld2_UNIQUE` (`saltOld2`),
  UNIQUE KEY `saltOld1_UNIQUE` (`saltOld1`),
  UNIQUE KEY `passwordOld1_UNIQUE` (`passwordOld1`),
  UNIQUE KEY `passwordOld2_UNIQUE` (`passwordOld2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('06ae4bed-bd10-4b58-9c90-fcf9b6fbcc93','a@a.com','G��QUY\�hۣ8�M;\�W�Y\�\�=�x\�Lr`\�9�KM��9	\�\"Xx\�\�@\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',NULL,NULL,'�KM��9	\�\"Xx\�\�@',NULL,NULL,'test','test',NULL,'','','',0,'user'),('94b6fb73-9b11-4ba7-b6d3-c24d60b5d641','admin@istrator.com','�\�!u|%_\"H�n�hD0`��<�lL�\�9m;))3L��S1f\�s\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',NULL,NULL,';))3L��S1f\�s\�',NULL,NULL,'admin','istrator',NULL,'','','',100,'admin'),('c82b8eee-18b4-4c08-aa97-91e35acdb178','a@b.com','\�t�*[J�V�)��\�]�]\�5�\���OD�I5<�\�\�\�5�\"W�q\�m�3E�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','a�����4 <\�\�~D\0�TT���\�\'\�w�V\�i�\�\�C3A�3$�\�\�>\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',NULL,'\�\�5�\"W�q\�m�3E�','i�\�\�C3A�3$�\�\�>',NULL,'test','test',NULL,NULL,NULL,NULL,100,'user');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-01 19:37:17
